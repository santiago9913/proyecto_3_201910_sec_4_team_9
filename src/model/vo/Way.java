package model.vo;



import model.estructuras.Queue;

public class Way {

	private Long id;

	private Queue<Long> nd; 

	public Way(Long id) {
		this.id = id;
		nd = new Queue<>();
	}

	public Way() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Queue<Long> getNd() {
		return nd;
	}

	public void setNd(Queue<Long> nd) {
		this.nd = nd;
	}



}
