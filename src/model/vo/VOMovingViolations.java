package model.vo;


/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations>
{
	/**
	 * Entero del id del objeto y total paid
	 */
	private int objectId;
	
	/**
	 * String de la locaci�n, 
	 */
	
	private String location;
	
	/**
	 * Latitud y Longitud 
	 */
	private double lat, lon;
	
	
	/**
	 * Metodo constructor
	 */
	public VOMovingViolations(int objectId, String location, double lat, double lon)
	{
		this.objectId = objectId;
		this.location = location;
		this.lat = lat; 
		this.lon = lon;
	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public int getObjectId() {
		// TODO Auto-generated method stub
		return objectId;
	}


	
	/**
	 * @return  - Dirección en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * Devuelve la latitud 
	 * @return latitud
	 */
	public double getLat() {
		return lat; 
	}
	
	/**
	 * Devuelve la longitud
	 * @return longitud
	 */
	public double getLon() {
		return lon; 
	}
	
	
	@Override
	public int compareTo(VOMovingViolations o) 
	{

		int comparacion = Long.compare(this.objectId, o.objectId); 
		return comparacion;


	}
	@Override
	public String toString()
	{
		// TODO Convertir objeto en String (representacion que se muestra en la consola)
		return "ObjectID:" + objectId  + " || Location:" + location + " || LatLong: " + "("+lat+","+lon+")";
	}
}
