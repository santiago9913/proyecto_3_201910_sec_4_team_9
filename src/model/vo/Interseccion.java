package model.vo;

import java.util.Iterator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import model.estructuras.Queue;

public class Interseccion implements Comparable<Interseccion>
{
	@Expose
	@SerializedName("id")
	private long id; 

	@Expose
	@SerializedName("lat")
	private double lat;

	@Expose
	@SerializedName("lon")
	private double lon; 

	@Expose
	@SerializedName("infractions")
	private long[] violations; 

	@Expose
	@SerializedName("adj")
	private long[] adj;

	boolean nuevo = true;

	private Queue<Long> vio = new Queue<>(); 

	public Interseccion(long id, double lat, double lon, long[] violations, long[] adj) {
		this.id = id; 
		this.lat = lat; 
		this.lon = lon; 
		this.violations = violations;
		this.adj = adj;
	}

	public Interseccion() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getLat() {
		return lat;
	}

	public long[] getViolations() {
		return violations;
	}

	public void setViolations(int size){

		this.violations = new long[size];
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public Queue<Long> getAdj() {
		Queue<Long> edge = new Queue<>(); 
		for(Long i : adj) {
			edge.enqueue(i);
		}
		return edge;
	}

	public long[] getAdjArray() {
		return adj; 
	}

	public void setAdj(int size) {
		this.adj = new long[size];
	}

	public Queue<Long> getVio() {
		return vio; 
	}

	public void addVio(long id) {
		if(vio == null) {
			vio = new Queue<>(); 
		}

		vio.enqueue(id);
	}

	public void toArray() {
		if(nuevo == true) {
			violations = null; 
			nuevo = false;
		}
		violations = new long[vio.size()]; 
		Iterator<Long> it = vio.iterator();
		int i = 0; 
		while(it.hasNext() && i < violations.length) {
			violations[i] = vio.dequeue();
			i++;
		}
	}

	@Override
	public int compareTo(Interseccion o) {
		// TODO Auto-generated method stub
		return 0;
	}


}
