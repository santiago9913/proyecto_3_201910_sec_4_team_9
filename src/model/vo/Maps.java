package model.vo;

import java.awt.BorderLayout;
import java.util.Iterator;

import javax.swing.JFrame;

import com.teamdev.jxmaps.ControlPosition;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.Polyline;
import com.teamdev.jxmaps.PolylineOptions;
import com.teamdev.jxmaps.swing.MapView;

import model.logic.MovingViolationsManager;

public class Maps extends MapView
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map map; 

	JFrame frame = new JFrame("Washigton DC");

	public Maps() {


		setOnMapReadyHandler(new MapReadyHandler() {

			@Override
			public void onMapReady(MapStatus status) {
				// TODO Auto-generated method stub
				if(status == MapStatus.MAP_STATUS_OK) {

					map = getMap();
					MapOptions mapOptions = new MapOptions(); 
					MapTypeControlOptions controlOptions = new MapTypeControlOptions(); 
					controlOptions.setPosition(ControlPosition.RIGHT_TOP);
					mapOptions.setMapTypeControlOptions(controlOptions);
					map.setOptions(mapOptions);
					map.setCenter(new LatLng(38.8963324, -77.0380200));
					map.setZoom(11);

					Iterable<Long> iterableKeys = MovingViolationsManager.getDataInter().keys();
					Iterator<Long> itKeys = iterableKeys.iterator();

					while(itKeys.hasNext()) {
						long key = itKeys.next();
						Iterator<Long> itVertex = MovingViolationsManager.getGraph().adj(key); 
						LatLng[] path = new LatLng[MovingViolationsManager.getGraph().getMyAdjList().get(key).size()];

						while(itVertex.hasNext()) {

							for(int i = 0; i < path.length; i++) {
								long first =  itVertex.next();
								Interseccion vertice = MovingViolationsManager.getDataInter().get(first);
								path[i] = new LatLng(vertice.getLat(), vertice.getLon()); 
							}
						}

						Polyline polyline = new Polyline(map); 
						polyline.setPath(path);

						PolylineOptions options = new PolylineOptions(); 
						options.setGeodesic(true);

						options.setStrokeColor("#8B0000");
						options.setStrokeOpacity(1.0);
						options.setStrokeWeight(2.0);

						polyline.setOptions(options);
					}

				}

			}
		});

		frame.add(this, BorderLayout.CENTER);
		frame.setSize(900, 600);
		frame.setVisible(true);
	}
}
