package model.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.teamdev.jxmaps.swing.MapView;

import model.estructuras.Graph;
import model.estructuras.Queue;
import model.estructuras.RedBlackBST;
import model.vo.Interseccion;
import model.vo.VOMovingViolations;
import model.vo.Way;

public class MovingViolationsManager extends MapView {


	//Constantes de rutas de archivos
	private static final String XML_CENTRAL_WASHINGTON = "./data/Central-WashingtonDC-OpenStreetMap.xml"; 
	private static final String XML_MAP = "./data/map.xml";
	private static final String SMALL_GRAPH ="./data/smallGraph.json";
	private static final String FINAL_GRAPH = "./data/finalGraph.json";
	private static final String MESES []= {
			"./data/January.csv",
			"./data/February.csv",
			"./data/March.csv",
			"./data/Abril.csv",
			"./data/May.csv",
			"./data/June.csv",
			"./data/July.csv",
			"./data/August.csv",
			"./data/September.csv",
			"./data/October.csv",
			"./data/November.csv",
			"./data/December.csv"


	};

	//Estructuras para manejo del grafo
	private static Graph<Long, Interseccion ,Double> graphData;
	private static RedBlackBST<Long, Interseccion> dataInter = new RedBlackBST<>();
	private static Queue<Way> dataWay = new Queue<>();
	private static RedBlackBST<Long, Interseccion> datosPintar = new RedBlackBST<Long, Interseccion>();

	private RedBlackBST<Double, Interseccion> datosCoordenadas = new RedBlackBST(); 

	//Estructuras para guardar las infracciones
	private RedBlackBST<Integer, VOMovingViolations> datosViolations;
	private int totalInfracciones;

	//Atributos que se usan al cargar
	private double minLatit, minLong, maxLatit, maxLong;
	private String temp; 
	private boolean contieneTag; 
	private boolean wayTag; 



	/**
	 * Metodo constructor
	 */
	public MovingViolationsManager()
	{
		//TODO inicializar los atributos
		graphData = new Graph<>(); 
		datosViolations = new RedBlackBST<>();
		minLatit = 0.0; 
		minLong = 0.0; 
		maxLatit = 0.0; 
		maxLong = 0.0;
		contieneTag = false;
		wayTag = false;
		totalInfracciones = 0;


	}

	public int darTotalInfracciones() {
		return totalInfracciones;
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void loadMovingViolations() throws Exception{


		for(int i=0 ; i < MESES.length; i++) {
			if(i == 0) {
				FileReader fileReaderMes = new FileReader(MESES[i]); 
				CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
				CSVReader csvReaderMes = new CSVReaderBuilder(fileReaderMes).withSkipLines(1).withCSVParser(parser).build();
				String[] row; 

				while((row = csvReaderMes.readNext()) != null) {
					int objectId = Integer.parseInt(row[0]); 
					String location = row[2];
					String latPre = row[17];
					String lonPre = row[18]; 
					latPre = latPre.replace(",", ".");
					lonPre = lonPre.replaceAll(",", ".");
					double lat = Double.parseDouble(latPre); 
					double lon = Double.parseDouble(lonPre);

					VOMovingViolations actual = new VOMovingViolations(objectId, location, lat, lon); 
					datosViolations.put(actual.getObjectId(), actual);
					totalInfracciones++;
				}
			}
			else {
				FileReader fileReaderMes = new FileReader(MESES[i]); 
				CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
				CSVReader csvReaderMes = new CSVReaderBuilder(fileReaderMes).withSkipLines(1).withCSVParser(parser).build();
				String[] row; 
				int aumenta = i >= 9 ? 1 : 0;

				while((row = csvReaderMes.readNext()) != null) {
					int objectId = Integer.parseInt(row[1]); 
					String location = row[2];
					String latPre = row[18 + aumenta];
					String lonPre = row[19 + aumenta]; 
					latPre = latPre.replace(",", ".");
					lonPre = lonPre.replaceAll(",", ".");
					double lat = Double.parseDouble(latPre);
					double lon = Double.parseDouble(lonPre);

					VOMovingViolations actual = new VOMovingViolations(objectId, location, lat, lon); 
					datosViolations.put(actual.getObjectId(), actual);
					totalInfracciones++;
				}

			}


		}
	}

	/**
	 * Lee el XML m�s peque�o 
	 * @throws Exception si no se pudo leer el archivo
	 */
	public void readSmallXML() throws Exception {
		File file = new File(XML_CENTRAL_WASHINGTON);
		SAXParserFactory factory = SAXParserFactory.newInstance(); 
		SAXParser saxParser = factory.newSAXParser(); 
		DefaultHandler handler = new DefaultHandler(){

			Interseccion interseccion; 
			Way way; 

			@Override
			public void characters(char[] buffer, int start, int lenght) {

				temp = new String(buffer, start, lenght);
			}

			@Override
			public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

				temp = ""; 

				//Lee la etiqueta bounds con los vertices del mapa
				if(qName.equals("bounds")) {
					minLatit = Double.parseDouble(attributes.getValue("minlat"));
					minLong = Double.parseDouble(attributes.getValue("minlon")); 
					maxLatit = Double.parseDouble(attributes.getValue("maxlat")); 
					maxLong = Double.parseDouble(attributes.getValue("maxlon"));
				}

				//Registra todoas las etiquetas <node> del XML
				else if(qName.equals("node")) {
					double lat = Double.parseDouble(attributes.getValue("lat"));
					double longi = Double.parseDouble(attributes.getValue("lon")); 
					Long id = new Long(attributes.getValue("id"));
					interseccion = new Interseccion(id, lat, longi, null, null);
				}	

				//Lee los atributos del tag <way>
				else if(qName.equals("way")) {
					wayTag = true; 
					Long id = new Long(attributes.getValue("id")); 
					way = new Way(id);
				}

				// Lee los atributos del tag <nd> en way 
				else if(qName.equals("nd")) {
					Long nd = new Long(attributes.getValue("ref"));
					way.getNd().enqueue(nd);
				}

				//Lee los atributos del tag <tag> en way 
				else if(qName.equals("tag") && wayTag == true) {
					String key = attributes.getValue("k");
					if(key.equals("highway")) {
						contieneTag = true;  
					}
				}
			}

			@Override
			public void endElement(String uri, String localName, String qName) throws SAXException {

				if(qName.equals("node")) {
					dataInter.put(interseccion.getId(),interseccion);
				}	
				if(qName.equals("way")) {
					if(contieneTag == true) { // Revisa que el <way> actual contenga el valor <Tag k= "highway">
						dataWay.enqueue(way);
						contieneTag = false; 
						wayTag = false; 
						Iterator<Long> it = way.getNd().iterator(); //Iterador para generar los arcos
						Iterator<Long> itVertices = way.getNd().iterator();

						Iterator<Long> itAdyacentes = way.getNd().iterator(); //Agrega los adyacentes a la interseccion

						boolean guardadoAnterior = false; 
						Long anterior = Long.valueOf(0); 

						while(itVertices.hasNext()) {
							Long firstKey = itVertices.next(); 
							Interseccion first = dataInter.get(firstKey); 
							if(!graphData.getMyVertices().contains(firstKey)) {
								datosPintar.put(first.getId(), first);
								datosCoordenadas.put(first.getLat(), first);
								graphData.addVertex(first.getId(), first);

							}
						}


						while(it.hasNext()){
							Long firstKey = it.next(); 
							double lat1 = dataInter.get(firstKey).getLat(); 
							double lon1 = dataInter.get(firstKey).getLon();

							if(!guardadoAnterior) {
								Long secondKey = it.next(); 
								anterior = secondKey; 	

								double lat2 = dataInter.get(secondKey).getLat(); 
								double lon2 = dataInter.get(secondKey).getLon(); 
								double info = Haversine.distance(lat1, lon1, lat2, lon2); 
								graphData.addEdge(firstKey, secondKey, info);	
								guardadoAnterior = true;
							}
							else if(guardadoAnterior) {
								double lat2Ant = dataInter.get(anterior).getLat(); 
								double lon2Ant = dataInter.get(anterior).getLon(); 
								double info = Haversine.distance(lat1, lon1, lat2Ant, lon2Ant); 

								graphData.addEdge(anterior, firstKey, info);
								anterior = firstKey;

							}	
						}
					}
					else {
						wayTag = false;
					}
				}
			}
		};
		saxParser.parse(file, handler);
		agregarAdyacentes();
		agregarInfraccionesAlGrafo();
		hacerElJSON();

	}

	public void agregarAdyacentes() {
		System.out.println("Agregando Adyacentes...");
		Iterable<Long> iterable = graphData.getMyVertices().keys(); 
		Iterator<Long> it = iterable.iterator(); 

		while(it.hasNext()) {
			long key = it.next();
			Iterator<Long> adj = graphData.adj(key);
			int size = graphData.getMyAdjList().get(key).size(); 
			Interseccion interseccion = datosPintar.get(key); 
			interseccion.setAdj(size);
			int i = 0; 
			while(adj.hasNext() && i< size) {
				long actual = adj.next(); 
				interseccion.getAdjArray()[i] = actual;
				i++; 
			}
		}

		System.out.println("�Se han agregado adyacentes!");
	}

	public void agregarInfraccionesAlGrafo() {

		System.out.println("Agregando Infracciones...");
		Iterable<Integer> infracciones = datosViolations.keys();
		Iterator<Integer> itInfracciones = infracciones.iterator();

		while(itInfracciones.hasNext()) {
			Integer keyActual = itInfracciones.next(); 

			VOMovingViolations actual = datosViolations.get(keyActual);
			double rangoCercano = actual.getLat();
			double lat1 = actual.getLat();
			double lon1 = actual.getLon();
			double weight = 0; 
			long lowestKey = 0;  

			//Rango a buscar la infraccion m�s cercana 
			Iterable<Double> vertices = datosCoordenadas.keysInRange(rangoCercano-0.2, rangoCercano+0.2); 
			Iterator<Double> itVertices = vertices.iterator();

			while(itVertices.hasNext()) {
				double key = itVertices.next(); 
				Interseccion inter = datosCoordenadas.get(key);
				double lat2 = inter.getLat();
				double lon2 = inter.getLon();

				double weightComp = Haversine.distance(lat1, lon1, lat2, lon2);

				if((weight == 0) || (weightComp < weight)) {
					weight = weightComp; 
					lowestKey = inter.getId();
				}

			}
			graphData.getMyVertices().get(lowestKey).getInfo().addVio(keyActual); //Informacion de la intesrseccion
		}

		Iterable<Long> finIter = graphData.getMyVertices().keys();
		Iterator<Long> fin = finIter.iterator();
		boolean end = false;

		System.out.println("Estamos Organizando Tus Datos...");

		while(fin.hasNext()) {

			Interseccion actual = graphData.getMyVertices().get(fin.next()).getInfo();

			//			if(actual.getVio().isEmpty() || actual.getVio() == null) {
			//				continue;
			//			}

			actual.toArray();
		}

		System.out.println("�Se han agregado infreacciones!");
	}


	/**
	 * Da el total de vertices del grafo 
	 * @return 
	 */
	public int darTotalVertices(){
		return graphData.V();

	}

	/**
	 * Da el total de arcos en el grafo
	 * @return
	 */
	public int darTotalArcos(){
		return graphData.E();
	}

	/**
	 * Retorna el arbol que contiene las intersecciones 
	 * @return arbol con todas las intersecciones
	 */
	public static RedBlackBST<Long, Interseccion> getDataInter(){
		return datosPintar;
	}

	/**
	 * Retorna el grafo 
	 * @return grafo con los datos
	 */
	public static Graph<Long, Interseccion, Double> getGraph() {
		return graphData;
	}

	/**
	 * 
	 */
	public void hacerElJSON ()
	{
		//Hago el JSON
		//Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();

		try {

			System.out.println("Creando JSON...");

			FileWriter writer = new FileWriter(SMALL_GRAPH); 
			Iterable<Long> iterable = graphData.getMyVertices().keys(); 
			Iterator<Long> it = iterable.iterator(); 

			while(it.hasNext()) {
				Interseccion actual = graphData.getMyVertices().get(it.next()).getInfo(); 
				String json = gson.toJson(actual); 
				writer.write(json);

			}

			writer.close();

		} catch (IOException ioe) {

		}

		System.out.println("�Se ha creado JSON!");

	}
	public void cargarJson()
	{

		graphData = null;
		try {
			Gson gson = new Gson(); 
			JsonReader reader = new JsonReader(new InputStreamReader(new FileInputStream(SMALL_GRAPH)));  
			reader.beginArray();
			graphData = new Graph<>(); 

			while(reader.hasNext()) {
				Interseccion actual = gson.fromJson(reader, Interseccion.class); 
				graphData.addVertex(actual.getId(), actual);
				datosPintar.put(actual.getId(), actual);
			}

			Iterable<Long> iterable = datosPintar.keys();
			Iterator<Long> itArbol = iterable.iterator();

			while(itArbol.hasNext()) {
				Interseccion actual = datosPintar.get(itArbol.next());
				double lat1 = actual.getLat(); 
				double lon1 = actual.getLon(); 
				Queue<Long> adj = actual.getAdj(); 
				Iterator<Long> it = adj.iterator(); 

				while(it.hasNext()) {
					Interseccion siguiente = datosPintar.get(it.next()); 
					double lat2 = siguiente.getLat(); 
					double lon2 = siguiente.getLon(); 
					double info = Haversine.distance(lat1, lon1, lat2, lon2); 
					graphData.addEdge(actual.getId(), siguiente.getId(), info);
				}


			}

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public Queue<Interseccion> caminoConMenorInfracciones(){
		return null; 
	}

	public Queue<Interseccion> interseccionesConMayorInfracciones(){
		return null;
	}

	public Queue<Interseccion> caminoEntreDosVerticesConMenorInfracciones(){
		return null;
	}

	public Queue<Interseccion> definirCuadricula(double latMin, double lonMin, double latMax, double lonMax){
		return null;
	}

}
