/**
 * Clase priority queue
 * Recuperado de presentaciones 
 */
package model.estructuras;

import java.util.Iterator;

public class PriorityQueue<Key extends Comparable<Key>>  implements IPriorityQueue<Key>
{
	private Key[] pq; 
	private int N;
	
	public PriorityQueue (int capacity) 
	{
		pq = (Key[]) new Comparable [ capacity ]; 
	}
	public PriorityQueue () 
	{
		pq = (Key[]) new Comparable [ 10 ]; 
	}
	public boolean isEmpty() 
	{ 
		return N == 0; 
	}
	
	public void add(Key x) 
	{ 
		if (N == pq.length)
		{
			 Key[] copia = pq;
             pq = (Key[]) new Comparable[ pq.length * 2];
             for ( int i = 0; i < copia.length; i++)
             {
              	 pq[i] = copia[i];
             } 
		}
		pq[N++] = x; 
	}
	public int size ()
	{
		return N;
	}
	public Key delMax()
	{
		int max = 0;
		for (int i = 1; i < N; i++)
			if (less(pq[max], pq[i])) max = i;
		exchange(pq ,max, N-1);
		return pq[--N];
	}
	public Key max()
	{
		int max = 0;
		for (int i = 1; i < N; i++)
			if (less(pq[max], pq[i])) max = i;
		return pq[max];
	}
	
	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(Comparable v, Comparable w)
	{
		
		return (v.compareTo(w) < 0);
	}
	
	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( Comparable[] datos, int i, int j)
	{
		Comparable k = datos[i];
		datos[i] = datos[j];
		datos[j] = k;
	}
	
	@Override
	public Iterator<Key> iterator() {
		return new Iterator() {
			private int index = 0;

			@Override
			public boolean hasNext() {
				return pq.length > index;
			}

			@Override
			public Object next() {
				return pq[index++];
			}
		};
	}
	

}
