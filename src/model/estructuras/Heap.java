package model.estructuras;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Heap<E extends Comparable<E>> implements Iterable<E>
{
	private int n; 
	private E[] pq;


	public Heap(int capacidadInicial)
	{
		pq = (E[]) new Comparable[capacidadInicial + 1];
		n = 0; 
	}

	public Heap() 
	{
		this(1);  
	}

	public boolean isEmpty()
	{
		return n == 0; 
	}

	public int size()
	{
		return n; 
	}

	public E max()
	{
		if(isEmpty())
			throw new NoSuchElementException("La cola de prioridad esta vacia"); 
		return pq[1];
	}

	public void resize(int capacity)
	{
		assert capacity > n;

		E[] temp = (E[]) new Comparable[capacity]; 

		for(int i = 1; i <= n; i++)
		{
			temp[i] = pq[i]; 
			pq[i] = null; 
		}

		pq = temp; 


	}

	public void insert(E element)
	{
		if(n == pq.length-1)
			resize(2*pq.length);

		pq[++n] = element;
		swim(n);
	}


	public E delMax()
	{
		if(isEmpty())
		{
			throw new NoSuchElementException("Cola de prioridad vacia"); 
		}

		E max = pq[1];
		exch(1, n--);
		sink(1);
		pq[n+1] = null;

		if((n > 0 ) && (n== (pq.length-1)/4))
		{
			resize(pq.length/2);
		}
		return max; 
	}

	private void sink(int k) 
	{
		while (2*k <= n) 
		{
			int j = 2*k;
			if (j < n && less(j, j+1)) j++;
			if (!less(k, j)) break;
			exch(k, j);
			k = j;
		}
	}

	private void swim(int k)
	{
		while (k > 1 && less(k/2, k)) 
		{
			exch(k, k/2);
			k = k/2;
		}
	}

	/**
	 * Dice si el valor comparado es menor con otro valor
	 * @param pq arreglo de datos
	 * @param i dato 1
	 * @param j dato 2
	 * @return true si el dato pq[i] es menor que pq[j] 
	 */
	private boolean less(int i, int j) {

		return ((Comparable<E>) pq[i]).compareTo(pq[j]) < 0;

	}

	/**
	 * Intercambia los elementos de un arrgelo
	 * @param pq arreglo donde se quiere realizar la operacion 
	 * @param i Para el caso de swim: el dato mayor
	 * @param j Para el caso de swim: el dato menor
	 */
	private void exch(int i, int j) 
	{
		E swap = pq[i];
		pq[i] = pq[j];
		pq[j] = swap;
	}


	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return new HeapIterator();
	}

	private class HeapIterator implements Iterator<E> {

		// create a new pq
		private Heap<E> copy;

		// add all items to copy of heap
		// takes linear time since already in heap order so no keys move
		public HeapIterator() {
			copy = new Heap<E>();
			for (int i = 1; i <= n; i++)
				copy.insert(pq[i]);
		}

		public boolean hasNext()  { return !copy.isEmpty();                     }

		public E next() {
			if (!hasNext()) throw new NoSuchElementException();
			return copy.delMax();
		}
	}


}
