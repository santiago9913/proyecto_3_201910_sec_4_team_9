


import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import model.estructuras.Stack;

public class TestStack  {

	private Stack <String> stack;
	@Before
	public void setUpEscenario1()
	{
		stack = new Stack <String>();
	}
	public void setUpEscenario2() 
	{
		stack = new Stack <String> ("a");
	}
	public void setUpEscenario3() 
	{
		stack = new Stack <String> ("a");
		stack.push("b");
		stack.push("c");
	}
	@Test
	public void constructor() 
	{
		assertEquals( 0,stack.size());
		assertTrue (stack.isEmpty());
		setUpEscenario2();
		assertEquals( 1, stack.size());
		assertTrue (!stack.isEmpty());
		
		
	}
	
	@Test
	public void enAndDestack() 
	{
		try
		{
			stack.push("a");
			assertEquals(1,stack.size());
				
			stack.push("e");
			assertEquals( 2,stack.size());
			
			stack.push("b");
			assertEquals( 3,stack.size());
			
			assertEquals("b",stack.pop());
			assertEquals( 2,stack.size());
			
			assertEquals( "e", stack.pop());
			assertEquals( 1, stack.size());
			
			assertEquals("a",stack.pop());
			assertEquals( 0,stack.size());
			
		}catch( Exception e)
		{
			fail("Deberia poder agregar");
		}
		try 
		{
			stack.pop();
			fail("No se puede eliminar");
		}catch( Exception e)
		{
			//Respuesta correcta
		}
		
	}
	
	@Test
	public void iterador() 
	{
		int i=0;
		setUpEscenario3();
		String letras []={"c","b","a"};
		Iterator<String> it = (Iterator<String> )stack.iterator();
		while (it.hasNext())
		{
			assertEquals(letras[i],it.next());
			i++;
			
		}
		assertEquals(i, stack.size());
	}

}
