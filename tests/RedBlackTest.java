

import static org.junit.Assert.*;



import org.junit.Before;
import org.junit.Test;
import model.estructuras.Queue;

import model.estructuras.RedBlackBST;

public class RedBlackTest {
	public RedBlackBST<Integer,Integer> rb;
	@Before
	public void setUpEscenario1()
	{
		rb = new RedBlackBST<Integer,Integer> ();
	}
	@Test
	public void  size()
	{
		assertEquals(rb.size(), 0);
		for (int i=1;i<=10;i++)
		{
			rb.put(i, i);
			assertEquals(rb.size(), i);
		}
	}
	@Test
	public void  isEmpty()
	{
		assertTrue(rb.isEmpty());
		rb.put(10, 5);
		assertFalse(rb.isEmpty());
	}
	@Test
	public void get()
	{
		
		assertEquals(rb.get(10),null);
		rb.put(100, 1);
		rb.put(20, 5);
		rb.put(30, 9);
		rb.put(40, 2);
		assertEquals(rb.get(40),(Integer)2);
		assertEquals(rb.get(100),(Integer)1);
		assertEquals(rb.get(20),(Integer)5);
		assertEquals(rb.get(30),(Integer)9);
		
	}

	@Test
	public void getHeight() 
	{
		assertEquals(rb.getHeight(10),-1);
		rb.put(100, 1);
		assertEquals(rb.getHeight(100),0);
		rb.put(20, 5);
		assertEquals(rb.getHeight(20),0);
		assertEquals(rb.getHeight(100),1);
		rb.put(30, 9);
		assertEquals(rb.getHeight(30),1);
		assertEquals(rb.getHeight(20),0);
		assertEquals(rb.getHeight(100),0);
		rb.put(40, 2);
		assertEquals(rb.getHeight(40),0);
	
	}
	@Test
	public void contains() 
	{
		assertFalse(rb.contains((Integer) 0));
		rb.put(100, 1);
		assertFalse(rb.contains((Integer) 10));
		assertTrue(rb.contains((Integer) 100));
		rb.put(200, 1);
		assertFalse(rb.contains((Integer) 10));
		assertTrue(rb.contains((Integer) 200));
		
	}
	@Test
	public void put()
	{
		for (int i=1;i<=10;i++)
		{
			rb.put(i, i);
			assertEquals(rb.size(), i);
		}
		for (int i=1;i<=10;i++)
			assertEquals(rb.get(i),(Integer)i);
		rb.put(2, 20);
		assertEquals(rb.get(2),(Integer)20);
		
	}
	
	@Test
	public void height()
	{
		assertEquals(rb.height(),-1);
		rb.put(100, 1);
		assertEquals(rb.height(),0);
		rb.put(20, 5);
		assertEquals(rb.height(),1);
		rb.put(30, 9);
		assertEquals(rb.height(),1);
		rb.put(40, 2);
		assertEquals(rb.height(),2);
	}
	
	@Test
	public void min() 
	{
		rb.put(101, 1);
		rb.put(100, 2);
		rb.put(104, 3);
		rb.put(110, 4);
		rb.put(102, 5);
		rb.put(103, 6);
		assertEquals(rb.min(), (Integer)100);
		rb.deleteMin();
		assertEquals(rb.min(), (Integer)101);
		rb.deleteMin();
		assertEquals(rb.min(), (Integer)102);
		rb.deleteMin();
		assertEquals(rb.min(), (Integer)103);
		
	}
	@Test
	public void max()
	{
		rb.put(101, 1);
		rb.put(100, 2);
		rb.put(104, 3);
		rb.put(110, 4);
		rb.put(102, 5);
		rb.put(103, 6);
		assertEquals(rb.max(), (Integer)110);
		rb.deleteMax();
		assertEquals(rb.max(), (Integer)104);
		rb.deleteMax();
		assertEquals(rb.max(), (Integer)103);
		rb.deleteMax();
		assertEquals(rb.max(), (Integer)102);
	}
	@Test
	public void keys ()
	{
		Iterable <Integer> res = rb.keys();
		assertTrue(((Queue <Integer>) res).isEmpty());
		rb.put(5, 1);
		res = rb.keys();
		assertEquals((Integer) 5,((Queue <Integer>) res).dequeue());
		for (int i=0;i<10;i++)
		{
			rb.put(i, 50-i);
		}
		res = rb.keys();
		boolean arreglo []= new boolean [10];
		assertEquals(rb.size(),((Queue <Integer>) res).size());
		for (int i=0;i<10;i++)
		{
			int j=(int)((Queue <Integer>) res).dequeue();
			if (arreglo[j]!=false)
			{
				fail("Aparece dos veces un valor");
			}
			arreglo[j]=true;
		}
		
	}
	@Test
	public void valuesInRange ()
	{
		Iterable <Integer> res = rb.valuesInRange((Integer) 10,(Integer) 20);
		assertTrue(((Queue <Integer>) res).isEmpty());
		for (int i=0;i<10;i++)
		{
			rb.put(50-i, i);
		}
		res = rb.valuesInRange((Integer) 45,(Integer) 50);
		boolean arreglo []= new boolean [6];
		assertEquals(6,((Queue <Integer>) res).size());
		for (int i=0;i<=5;i++)
		{
			int j=(int)((Queue <Integer>) res).dequeue();
			if (arreglo[j]!=false)
			{
				fail("Aparece dos veces un valor");
			}
			arreglo[j]=true;
		}
	}
	@Test
	public void keysInRange ()
	{
		Iterable <Integer> res = rb.keysInRange((Integer) 10,(Integer) 20);
		assertTrue(((Queue <Integer>) res).isEmpty());
		for (int i=0;i<10;i++)
		{
			rb.put(i, 50-i);
		}
		res = rb.keysInRange((Integer) 5,(Integer) 10);
		boolean arreglo []= new boolean [10];
		assertEquals(5,((Queue <Integer>) res).size());
		for (int i=0;i<5;i++)
		{
			int j=(int)((Queue <Integer>) res).dequeue();
			if (arreglo[j]!=false)
			{
				fail("Aparece dos veces un valor");
			}
			arreglo[j]=true;
		}
	}
}
